<?php

/**
 * Main template file
 * 
 * PHP version 7.2
 * 
 * @category  Template
 * @package   PackageName
 * @author    Original Author <author@example.com>
 * @copyright 1997-2005 The PHP Group
 * @license   http://www.php.net/license/3_01.txt  PHP License 3.01
 * @link      http://pear.php.net/package/PackageName
 */

get_header(); ?>

    <h1><?php bloginfo('name'); ?></h1>
    
    <h2>My name is Webpack</h2>
    <ul>
      <li>Download the installer to the current directory</li>
      <li>Verify the installer SHA-384, which you can also cross-check here</li>
      <li>Run the installer</li>
      <li>Remove the installer</li>
    </ul>

<?php get_footer(); ?>