<?php 

/**
 * Footer 
 * 
 * PHP version 7.2
 * 
 * @category  Template_Part
 * @package   Theme
 * @author    Original Author <author@example.com>
 * @copyright 1997-2005 The PHP Group
 * @license   http://www.php.net/license/3_01.txt  PHP License 3.01
 * @link      http://pear.php.net/package/PackageName
 */

wp_footer();

?>
</body>
</html>