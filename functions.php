<?php

/**
 * Theme functions and definitions.
 * 
 * PHP version 7.2
 * 
 * @category  ThemeFunctions
 * @package   Webpack-theme
 * @author    Oleg Draganchuk <oleg.draganchuk@gmail.com>
 * @copyright 1997-2005 The PHP Group
 * @license   http://www.php.net/license/3_01.txt  PHP License 3.01
 * @link      http://pear.php.net/package/PackageName
 */

/**
 * Text domain definition
 */

defined('THEME_TD') ? THEME_TD : define('THEME_TD', 'our-way-tours');

// Load modules

$theme_includes = [
    '/lib/enqueue-scripts.php', // Enqueue styles and scripts
];

foreach ($theme_includes as $file) {
    if (!$filepath = locate_template($file)) {
        continue;
        trigger_error(
            sprintf(__('Error locating %s for inclusion', THEME_TD), $file),
            E_USER_ERROR
        );
    }

    include_once $filepath;
}
unset($file, $filepath);


/**
 * Theme the TinyMCE editor (Copy post/page text styles in this file)
 */
add_editor_style('assets/dist/css/custom-editor-style.css');

/**
 * Custom CSS for the login page
 *
 * @return void
 */
function loginCSS() 
{
    echo '<link 
        rel="stylesheet" 
        type="text/css" 
        href="'
        . get_template_directory_uri(THEME_TD) . 'assets/dist/css/wp-login.css"/>';
}
add_action('login_head', 'loginCSS');
